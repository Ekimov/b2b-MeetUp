package com.example.igorx.b2b_meetup.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class DataUser implements Serializable {

    @SerializedName("method_name")
    @Expose
    private String methodName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    /**
     *
     * @return
     * The methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     *
     * @param methodName
     * The method_name
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

}

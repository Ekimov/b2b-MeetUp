package com.example.igorx.b2b_meetup.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.igorx.b2b_meetup.R;
import com.example.igorx.b2b_meetup.models.DataUser;
import com.example.igorx.b2b_meetup.views.B2BProgressBar;

import java.util.ArrayList;

/**
 * Created by IgorX on 27.01.2016.
 */
public class ChatsPagerAdapter extends PagerAdapter {

    private Activity context;
    private ArrayList<DataUser> users;
    private ListView lvMeetUp;
    private MeetUpListAdapter adapter;
    private B2BProgressBar progressBar;

    public ChatsPagerAdapter(Activity context, ArrayList<DataUser> users) {
        this.context=context;
        this.users=users;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return 5;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
           View view=null;
        switch (position){
            case 0:
                view = context.getLayoutInflater().inflate(R.layout.listview_layout, null);
                lvMeetUp = (ListView) view.findViewById(R.id.lvMeetUp);
                adapter = new MeetUpListAdapter(context, users);
                lvMeetUp.setAdapter(adapter);
                container.addView(view);
                break;
            case 1:

                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;

        }
//           if(position==0) {
//               view = context.getLayoutInflater().inflate(R.layout.listview_layout, null);
//               lvMeetUp = (ListView) view.findViewById(R.id.lvMeetUp);
//               adapter = new MeetUpListAdapter(context, users);
//               lvMeetUp.setAdapter(adapter);
//               container.addView(view);
//           }

           return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return o==view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager)container).removeView((View) object);
    }
}

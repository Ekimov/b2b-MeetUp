package com.example.igorx.b2b_meetup;

import android.app.Application;
import android.util.Log;

import com.example.igorx.b2b_meetup.utils.Consts;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by IgorX on 26.01.2016.
 */
public class MyApplication extends Application {


    public static API service;

    @Override
    public void onCreate() {
        super.onCreate();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://bt.b2bmeetup.com/api/bt/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service=retrofit.create(API.class);
    }
}

package com.example.igorx.b2b_meetup;

import com.example.igorx.b2b_meetup.models.DataUser;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by IgorX on 26.01.2016.
 */
public interface API {

    @FormUrlEncoded
    @POST("login/")
    Call<DataUser> login(@Field("email") String email,
                         @Field("password") String password);


}

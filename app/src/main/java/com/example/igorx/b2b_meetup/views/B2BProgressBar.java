package com.example.igorx.b2b_meetup.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.igorx.b2b_meetup.R;

/**
 * Created by IgorX on 30.01.2016.
 */
public class B2BProgressBar extends FrameLayout {

    public ImageView outerCircle, innerCircle;
    public B2BProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public B2BProgressBar(Context context) {
        super(context);
        init();
    }

    private void init() {
        View view=LayoutInflater.from(getContext()).inflate(R.layout.progress_bar,null);
        outerCircle= (ImageView) view.findViewById(R.id.imOuterCircle);
        innerCircle= (ImageView) view.findViewById(R.id.imInnerCircle);
        final Animation animationRotateOuter = AnimationUtils.loadAnimation(
                getContext(), R.anim.rotate_outer);
        final Animation animationRotateInner = AnimationUtils.loadAnimation(
                getContext(), R.anim.rotate_inner);
        outerCircle.startAnimation(animationRotateOuter);
        innerCircle.startAnimation(animationRotateInner);
        addView(view);
    }
}

package com.example.igorx.b2b_meetup.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.igorx.b2b_meetup.R;

/**
 * Created by IgorX on 06.02.2016.
 */
public class HideFlipFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.hide_flip_fragment,container,false);
        getChildFragmentManager().beginTransaction().add(R.id.parentContainer,MeetUpFragment.newInstance(null));
        return view;
    }
}

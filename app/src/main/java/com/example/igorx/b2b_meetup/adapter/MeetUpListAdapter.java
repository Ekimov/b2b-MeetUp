package com.example.igorx.b2b_meetup.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igorx.b2b_meetup.R;
import com.example.igorx.b2b_meetup.models.Data;
import com.example.igorx.b2b_meetup.models.DataUser;

import java.util.ArrayList;

/**
 * Created by IgorX on 30.01.2016.
 */
public class MeetUpListAdapter extends BaseAdapter {
    ArrayList<DataUser> users;
    Activity context;

    public MeetUpListAdapter(Activity context,ArrayList<DataUser> users) {
        this.context=context;
        this.users=users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
         DataUser user = users.get(position);
        Log.i("igor","user in adapter= "+user);
         Data dataUser=user.getData();
        if(convertView==null){
            convertView=context.getLayoutInflater().inflate(R.layout.item_list_meetup_layaut,parent,false);
            viewHolder=new ViewHolder();
            viewHolder.tvName= (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvRoleName= (TextView) convertView.findViewById(R.id.tvStatus);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        if(viewHolder.tvName!=null) {
            viewHolder.tvName.setText(dataUser.getFirstName() + " " + dataUser.getLastName() + " (" + dataUser.getCountryName() + ") ");
        }
        if(viewHolder.tvRoleName!=null) {
            viewHolder.tvRoleName.setText(dataUser.getRoleName()+", "+dataUser.getCompany());
        }
        return convertView;
    }


    public static class ViewHolder {
        TextView tvName, tvRoleName;
        ImageView info;
    }
}

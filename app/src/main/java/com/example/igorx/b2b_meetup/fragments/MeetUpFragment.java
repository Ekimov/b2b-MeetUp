package com.example.igorx.b2b_meetup.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.igorx.b2b_meetup.AdvertiserService;
import com.example.igorx.b2b_meetup.LoginActivity;
import com.example.igorx.b2b_meetup.R;
import com.example.igorx.b2b_meetup.adapter.MeetUpListAdapter;
import com.example.igorx.b2b_meetup.models.DataUser;
import com.example.igorx.b2b_meetup.utils.Consts;
import com.example.igorx.b2b_meetup.views.B2BProgressBar;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MeetUpFragment extends Fragment implements View.OnClickListener {


    private ListView lvMeetUp;

    private ArrayList<DataUser> users;
    private DataUser user;
    private B2BProgressBar progressBar;
    private TextView tvName, tvRoleName;
    private LinearLayout llHeader;

    private static final String USERS = "users";


    ///////////////BLE////
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private MeetUpListAdapter adapter;
    private BluetoothLeScanner mLEScanner;
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private List<ScanFilter> filters;
    private ScanSettings settings;
    private FrameLayout flDevices;
    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private Intent adverIntent;
    private boolean isScanning;
    private LinearLayout llAccount;
    private ParcelUuid[] uuids;

    private ArrayList<ParcelUuid> uuidList;
    public static MeetUpFragment newInstance(DataUser user) {
        MeetUpFragment fragment = new MeetUpFragment();
        Bundle args = new Bundle();
        args.putSerializable(USERS, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (DataUser) getArguments().getSerializable(USERS);
            adverIntent=new Intent(getActivity(), AdvertiserService.class);
            startAdvertising();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meet_up, container, false);
        isScanning=true;
        initView(view);
        searchBLEDevice();
        return view;
    }

    private void startAdvertising() {
        getActivity().startService(adverIntent);
    }

    private void searchBLEDevice() {

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            else{
                scanLeDevice(true);
            }
        }

        mLeDeviceListAdapter = new LeDeviceListAdapter();
        lvMeetUp.setAdapter(mLeDeviceListAdapter);
        scanLeDevice(true);
    }

    private void initView(View view) {
        mHandler = new Handler();
//        flDevices= (FrameLayout) view.findViewById(R.id.fl_device_list);
//        flDevices.setOnClickListener(this);
        llAccount= (LinearLayout) view.findViewById(R.id.llAccount);
        llHeader= (LinearLayout) view.findViewById(R.id.llFlip);
        llHeader.setOnClickListener(this);
        lvMeetUp = (ListView) view.findViewById(R.id.lvMeetUp);
        progressBar = (B2BProgressBar) view.findViewById(R.id.progress_bar);
        tvName= (TextView) view.findViewById(R.id.tvName);
        tvRoleName= (TextView)view.findViewById(R.id.tvStatus);
        if(user!=null){
            llAccount.setVisibility(View.VISIBLE);
            tvName.setText(user.getData().getFirstName() + " " + user.getData().getLastName() + " (" + user.getData().getCountryName() + ") ");
            tvRoleName.setText(user.getData().getRoleName() + ", " + user.getData().getCompany());
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            getActivity().finish();
            return;
        }
       // BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
//        Method getUuidsMethod = null;
//        try {
//            getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        }
//        try {
//            uuids = (ParcelUuid[]) getUuidsMethod.invoke(mBluetoothAdapter, null);
//            uuidList=new ArrayList<>(Arrays.asList(uuids));
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            getActivity().finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            progressBar.setVisibility(View.VISIBLE);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            progressBar.setVisibility(View.GONE);
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopAdvertising();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llFlip:
                isScanning=!isScanning;
                if(isScanning){
                    searchBLEDevice();
                }
                else{
                scanLeDevice(isScanning);}
                break;
        }
    }

    private class LeDeviceListAdapter extends BaseAdapter {
        ParcelUuid[] phoneUuids=null;
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = getActivity().getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);

            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            BluetoothDevice device = mLeDevices.get(i);
            if (view == null) {
                view = mInflator.inflate(R.layout.item_list_meetup_layaut, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.tvStatus);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.tvName);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else {
                viewHolder.deviceName.setText(R.string.unknown_device);
            }

            viewHolder.deviceAddress.setText(device.getAddress());

           BluetoothGatt bGatt=device.connectGatt(getActivity(), false, new BluetoothGattCallback() {
               @Override
               public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                   super.onServicesDiscovered(gatt, status);
                   Log.i("igor", "uuidssss= " );
               }

               @Override
                public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    super.onCharacteristicRead(gatt, characteristic, status);
                    Log.i("igor", "uuid= " + characteristic.getUuid());
                }
            });
            bGatt.connect();
            if(bGatt.getServices().size()!=0) {
                Log.i("igor", "bGatt.getServices().get(0).getUuid()= " + bGatt.getServices().get(0).getUuid());
            }
//            List<BluetoothGattService> gattServices=bGatt.getServices();
//            if(gattServices.size()!=0) {
//                Log.i("igor", "uuid gatt= " + gattServices.get(0));
//            }
//            if(uuids!=null && i<uuids.length) {
//                viewHolder.deviceAddress.setText(device.ACTION_UUID);
//

            return view;
        }
    }



    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {


                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, final byte[] scanRecord) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLeDeviceListAdapter.addDevice(device);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };

    private String parseUUID(byte[] scanRecord) {
        StringBuilder mSbUUID = new StringBuilder();
        for (int i = 0; i < scanRecord.length; i++) {
            // UUID
            if (i >= 10 & i <= 24) {
                if (Integer.toHexString(
                        scanRecord[i]).contains("ffffff")) {
                    mSbUUID.append(Integer.toHexString(scanRecord[i]).replace("ffffff", "") + "-");
                } else {
                    mSbUUID.append(Integer.toHexString(scanRecord[i]) + "-");
                }
            }
        }
        return mSbUUID.toString();
    }

    private String getStringUUID(byte[] scanRecord) {
        String uuid=null;
        int startByte = 2;
        boolean patternFound = false;
        while (startByte <= 5) {
            if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                    ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                patternFound = true;
                break;
            }
            startByte++;
        }

        if (patternFound) {
            //Convert to hex String
            byte[] uuidBytes = new byte[16];
            System.arraycopy(scanRecord, startByte+4, uuidBytes, 0, 16);
            String hexString = bytesToHex(uuidBytes);

            //Here is your UUID
                    uuid =  hexString.substring(0,8) + "-" +
                    hexString.substring(8,12) + "-" +
                    hexString.substring(12,16) + "-" +
                    hexString.substring(16,20) + "-" +
                    hexString.substring(20,32);
                    Log.i("igor","new uuid= "+uuid);
            //Here is your Major value
            int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);
            Log.i("igor","major= "+major);
            //Here is your Minor value
            int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);
            Log.i("igor","minor= "+minor);
        }
        return uuid;
    }

    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }

    private void stopAdvertising() {
        getActivity().stopService(adverIntent);
    }
}

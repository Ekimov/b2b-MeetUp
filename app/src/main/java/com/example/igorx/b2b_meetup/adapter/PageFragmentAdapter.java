package com.example.igorx.b2b_meetup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.igorx.b2b_meetup.fragments.AgendaFragment;
import com.example.igorx.b2b_meetup.fragments.ContactsFragment;
import com.example.igorx.b2b_meetup.fragments.FindWhoFragment;
import com.example.igorx.b2b_meetup.fragments.HideFlipFragment;
import com.example.igorx.b2b_meetup.fragments.MeetUpFragment;
import com.example.igorx.b2b_meetup.fragments.MyProfileFragment;
import com.example.igorx.b2b_meetup.fragments.ParentMeetUp;
import com.example.igorx.b2b_meetup.models.DataUser;

import java.util.ArrayList;

/**
 * Created by IgorX on 31.01.2016.
 */
public class PageFragmentAdapter extends FragmentPagerAdapter {

    ArrayList<DataUser> users;
    DataUser user;
    public PageFragmentAdapter(FragmentManager fm, DataUser user) {
        super(fm);
       this.user=user;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
               return MeetUpFragment.newInstance(user);
            case 1:
               return FindWhoFragment.newInstance("","");
            case 2:
              return   MyProfileFragment.newInstance("","");
            case 3:
              return   AgendaFragment.newInstance("","");
            case 4:
               return ContactsFragment.newInstance("","");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}

package com.example.igorx.b2b_meetup.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Data implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("company_size")
    @Expose
    private Object companySize;
    @SerializedName("company_turnover")
    @Expose
    private Object companyTurnover;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("website")
    @Expose
    private Object website;
    @SerializedName("skills")
    @Expose
    private List<Object> skills = new ArrayList<Object>();
    @SerializedName("sector_id")
    @Expose
    private List<Object> sectorId = new ArrayList<Object>();
    @SerializedName("role_id")
    @Expose
    private String roleId;
    @SerializedName("profile_image")
    @Expose
    private Object profileImage;
    @SerializedName("profile_brochure_name")
    @Expose
    private Object profileBrochureName;
    @SerializedName("profile_brochure")
    @Expose
    private Object profileBrochure;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("products")
    @Expose
    private List<Object> products = new ArrayList<Object>();
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("phone_code_id")
    @Expose
    private Object phoneCodeId;
    @SerializedName("phone_code")
    @Expose
    private Object phoneCode;
    @SerializedName("phone_num")
    @Expose
    private Object phoneNum;
    @SerializedName("phone")
    @Expose
    private Object phone;
    @SerializedName("role_name")
    @Expose
    private String roleName;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("company_size_name")
    @Expose
    private Object companySizeName;
    @SerializedName("company_turnover_name")
    @Expose
    private Object companyTurnoverName;
    @SerializedName("device_id")
    @Expose
    private Object deviceId;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The company
     */
    public String getCompany() {
        return company;
    }

    /**
     *
     * @param company
     * The company
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     *
     * @return
     * The companySize
     */
    public Object getCompanySize() {
        return companySize;
    }

    /**
     *
     * @param companySize
     * The company_size
     */
    public void setCompanySize(Object companySize) {
        this.companySize = companySize;
    }

    /**
     *
     * @return
     * The companyTurnover
     */
    public Object getCompanyTurnover() {
        return companyTurnover;
    }

    /**
     *
     * @param companyTurnover
     * The company_turnover
     */
    public void setCompanyTurnover(Object companyTurnover) {
        this.companyTurnover = companyTurnover;
    }

    /**
     *
     * @return
     * The countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     *
     * @param countryId
     * The country_id
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     *
     * @return
     * The website
     */
    public Object getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(Object website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The skills
     */
    public List<Object> getSkills() {
        return skills;
    }

    /**
     *
     * @param skills
     * The skills
     */
    public void setSkills(List<Object> skills) {
        this.skills = skills;
    }

    /**
     *
     * @return
     * The sectorId
     */
    public List<Object> getSectorId() {
        return sectorId;
    }

    /**
     *
     * @param sectorId
     * The sector_id
     */
    public void setSectorId(List<Object> sectorId) {
        this.sectorId = sectorId;
    }

    /**
     *
     * @return
     * The roleId
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     *
     * @param roleId
     * The role_id
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     *
     * @return
     * The profileImage
     */
    public Object getProfileImage() {
        return profileImage;
    }

    /**
     *
     * @param profileImage
     * The profile_image
     */
    public void setProfileImage(Object profileImage) {
        this.profileImage = profileImage;
    }

    /**
     *
     * @return
     * The profileBrochureName
     */
    public Object getProfileBrochureName() {
        return profileBrochureName;
    }

    /**
     *
     * @param profileBrochureName
     * The profile_brochure_name
     */
    public void setProfileBrochureName(Object profileBrochureName) {
        this.profileBrochureName = profileBrochureName;
    }

    /**
     *
     * @return
     * The profileBrochure
     */
    public Object getProfileBrochure() {
        return profileBrochure;
    }

    /**
     *
     * @param profileBrochure
     * The profile_brochure
     */
    public void setProfileBrochure(Object profileBrochure) {
        this.profileBrochure = profileBrochure;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The products
     */
    public List<Object> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<Object> products) {
        this.products = products;
    }

    /**
     *
     * @return
     * The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The phoneCodeId
     */
    public Object getPhoneCodeId() {
        return phoneCodeId;
    }

    /**
     *
     * @param phoneCodeId
     * The phone_code_id
     */
    public void setPhoneCodeId(Object phoneCodeId) {
        this.phoneCodeId = phoneCodeId;
    }

    /**
     *
     * @return
     * The phoneCode
     */
    public Object getPhoneCode() {
        return phoneCode;
    }

    /**
     *
     * @param phoneCode
     * The phone_code
     */
    public void setPhoneCode(Object phoneCode) {
        this.phoneCode = phoneCode;
    }

    /**
     *
     * @return
     * The phoneNum
     */
    public Object getPhoneNum() {
        return phoneNum;
    }

    /**
     *
     * @param phoneNum
     * The phone_num
     */
    public void setPhoneNum(Object phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     *
     * @return
     * The phone
     */
    public Object getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(Object phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     *
     * @param roleName
     * The role_name
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     *
     * @return
     * The countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     *
     * @param countryName
     * The country_name
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     *
     * @return
     * The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     *
     * @param countryCode
     * The country_code
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     *
     * @return
     * The companySizeName
     */
    public Object getCompanySizeName() {
        return companySizeName;
    }

    /**
     *
     * @param companySizeName
     * The company_size_name
     */
    public void setCompanySizeName(Object companySizeName) {
        this.companySizeName = companySizeName;
    }

    /**
     *
     * @return
     * The companyTurnoverName
     */
    public Object getCompanyTurnoverName() {
        return companyTurnoverName;
    }

    /**
     *
     * @param companyTurnoverName
     * The company_turnover_name
     */
    public void setCompanyTurnoverName(Object companyTurnoverName) {
        this.companyTurnoverName = companyTurnoverName;
    }

    /**
     *
     * @return
     * The deviceId
     */
    public Object getDeviceId() {
        return deviceId;
    }

    /**
     *
     * @param deviceId
     * The device_id
     */
    public void setDeviceId(Object deviceId) {
        this.deviceId = deviceId;
    }

}
package com.example.igorx.b2b_meetup.utils;


import android.os.ParcelUuid;

public class Consts {
    public static final String BASE_URL="http://bt.b2bmeetup.com/api/bt/";
    public static final String SUCCESS_STATUS="success";
    public static final String FAILED_STATUS="failed";

    public static final ParcelUuid Service_UUID = ParcelUuid
            .fromString("0000b81d-0000-1000-8000-00805f9b34fb");

}

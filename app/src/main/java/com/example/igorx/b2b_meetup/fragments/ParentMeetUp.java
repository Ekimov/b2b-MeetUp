package com.example.igorx.b2b_meetup.fragments;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.igorx.b2b_meetup.R;

/**
 * Created by IgorX on 06.02.2016.
 */
public class ParentMeetUp extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.parent_meetup_fragment,container,false);
        return view;
    }
}

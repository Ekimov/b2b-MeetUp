package com.example.igorx.b2b_meetup;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.igorx.b2b_meetup.models.DataUser;
import com.loopj.android.http.*;

import com.example.igorx.b2b_meetup.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private Button btnSignin;
    private EditText etEmail,etPass;
    private Call<DataUser> loginCall;
    private DataUser dataUser;
    private Intent intent;
    private ArrayList<DataUser> users=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        initView();
    }

    private void initView() {
        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnSignin= (Button) findViewById(R.id.btnSignUp);
        etEmail= (EditText) findViewById(R.id.edUserName);
        etPass= (EditText) findViewById(R.id.edPassword);
        btnSignin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
            createLoginRequest();
              break;
            case R.id.btnSignUp:
                intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
        }
    }


    private void createLoginRequest() {
        if (!TextUtils.isEmpty(etEmail.getText().toString()) || !TextUtils.isEmpty(etPass.getText().toString())) {
            loginCall = MyApplication.service.login(etEmail.getText().toString(),etPass.getText().toString() );//etEmail.getText().toString(), etPass.getText().toString());
            loginCall.enqueue(new Callback<DataUser>() {
                @Override
                public void onResponse(Response<DataUser> response, Retrofit retrofit) {
                    Log.i("igor", "response= " + response.body());
                    dataUser = response.body();
                    switch (dataUser.getStatus()) {
                        case Consts.FAILED_STATUS:
                            Toast.makeText(LoginActivity.this, "Ошибка аутентификации!", Toast.LENGTH_LONG).show();
                            break;
                        case Consts.SUCCESS_STATUS:
                            intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("user", dataUser);
                            startActivity(intent);
                            LoginActivity.this.finish();
                            break;
                    }
//                   if(dataUser.getStatus().equals(Consts.FAILED_STATUS)){
//                       Toast.makeText(LoginActivity.this, "Ошибка аутентификации!", Toast.LENGTH_LONG).show();
//                    }
//
//                    users.add(dataUser);
//                    intent = new Intent(LoginActivity.this, MainActivity.class);
//                    intent.putExtra("users", users);
//                    startActivity(intent);
//                    LoginActivity.this.finish();
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.i("igor", "onFailure= " + t.toString());
                    Toast.makeText(LoginActivity.this, "Ошибка аутентификации!", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(LoginActivity.this, "Все поля должны быть заполнены", Toast.LENGTH_LONG).show();
        }
    }

    public class RequestCommentsAsync extends AsyncTask<Void, Void, Void> {

        JSONObject postJsonLogin;

        public RequestCommentsAsync(JSONObject postJsonLogin){
            this.postJsonLogin =postJsonLogin;
        }

        @Override
        protected Void doInBackground(Void... params) {


            Log.i("igor","json= "+postJsonLogin);
            try {
               String loadData=getContent(postJsonLogin);
                Log.i("igor", "Loading DATA= " + loadData);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    private String getContent(JSONObject postJsonLogin) throws IOException {

        BufferedReader reader = null;
        try {
            URL url = new URL(Consts.BASE_URL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("POST");
            c.setReadTimeout(10000);
            c.setDoInput(true);
            c.setDoOutput(true);
            OutputStream os = c.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os));
            writer.write( postJsonLogin.toString());
            writer.flush();
            writer.close();
            os.close();
            c.connect();
            Log.i("igor", "c.getInputStream()= "+c.getInputStream());
            reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            Log.i("igor","read= "+reader.toString());
            StringBuilder buf = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            return (buf.toString());
        }finally {
            if (reader!= null) {
                reader.close();
            }
        }
    }


}

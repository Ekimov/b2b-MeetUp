package com.example.igorx.b2b_meetup;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.igorx.b2b_meetup.adapter.ChatsPagerAdapter;
import com.example.igorx.b2b_meetup.adapter.PageFragmentAdapter;
import com.example.igorx.b2b_meetup.models.DataUser;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ViewPager viewPager;
    private ImageView circle;
    private ChatsPagerAdapter adapter;
    private PageFragmentAdapter pageAdapter;
    private int colorTabActive, colorTabNoneActive;
    private TextView tvMeetUp, tvFindWho, tvMyProfile, tvAgenda, tvContacts;
    private LinearLayout llMeetUp,llFindWho, llMyProfile, llAgenda, llContacts;
    private ImageView ivMeetUp, ivMyProfile, ivFindWho, ivAgenda, ivContacts;
    private Drawable[] imagesActive;
    private Drawable[] imagesNoneActive;
    private ArrayList<DataUser> users;
    private DataUser user;


    private TextView [] textViews;
    private ImageView [] imageViews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imagesActive=new Drawable[]{getResources().getDrawable(R.drawable.ic_contacts_active),
                getResources().getDrawable(R.drawable.find_who_active),
                getResources().getDrawable(R.drawable.ic_profile_active),
                getResources().getDrawable(R.drawable.ic_agenda_active),
                getResources().getDrawable(R.drawable.ic_contacts_active)};

        imagesNoneActive=new Drawable[]{getResources().getDrawable(R.drawable.ic_contacts_none_active),
                getResources().getDrawable(R.drawable.ic_find_who_non_active),
                getResources().getDrawable(R.drawable.ic_profile_none_active),
                getResources().getDrawable(R.drawable.ic_agenda_non_active),
                getResources().getDrawable(R.drawable.ic_contacts_none_active)};
        setContentView(R.layout.layout_main_activity);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }
        user= (DataUser) getIntent().getSerializableExtra("user");
        Log.i("igor", "users in MainActivity= " + users);
        initView();
    }



    private void initView() {
        initTabs();
        pageAdapter=new PageFragmentAdapter(getSupportFragmentManager(),user);
        //adapter=new ChatsPagerAdapter(this,users);
        viewPager= (ViewPager) findViewById(R.id.chats_pager);
        viewPager.setAdapter(pageAdapter);
        viewPager.setCurrentItem(0);
        changeTabs(0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                    changeTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initTabs() {
       colorTabActive=getResources().getColor(R.color.tab_active);
       colorTabNoneActive=getResources().getColor(R.color.tab_non_active);
        llMeetUp= (LinearLayout) findViewById(R.id.llMeetUp);
        llFindWho= (LinearLayout) findViewById(R.id.llFindWho);
        llMyProfile= (LinearLayout) findViewById(R.id.llMyProfile);
        llAgenda= (LinearLayout) findViewById(R.id.llAgenda);
        llContacts= (LinearLayout) findViewById(R.id.llContacts);
        llMeetUp.setOnClickListener(this);
        llFindWho.setOnClickListener(this);
        llMyProfile.setOnClickListener(this);
        llAgenda.setOnClickListener(this);
        llContacts.setOnClickListener(this);

        ivMeetUp= (ImageView) findViewById(R.id.ivMeetUp);
        ivFindWho= (ImageView) findViewById(R.id.ivFindWho);
        ivMyProfile= (ImageView) findViewById(R.id.ivMyProfile);
        ivAgenda= (ImageView) findViewById(R.id.ivAgenda);
        ivContacts= (ImageView) findViewById(R.id.ivContacts);
        imageViews=new ImageView[]{ivMeetUp,ivFindWho,ivMyProfile,ivAgenda,ivContacts};


        tvMeetUp= (TextView) findViewById(R.id.tvMeetUp);
        tvFindWho= (TextView) findViewById(R.id.tvFindWho);
        tvMyProfile= (TextView) findViewById(R.id.tvMyProfile);
        tvAgenda= (TextView) findViewById(R.id.tvAgenda);
        tvContacts= (TextView) findViewById(R.id.tvContacts);
        textViews = new TextView[]{tvMeetUp,tvFindWho,tvMyProfile,tvAgenda,tvContacts};
    }

    private void changeTabs(int position){
       for(int i=0; i<textViews.length;i++){
           textViews[i].setTextColor(colorTabNoneActive);
           imageViews[i].setImageDrawable(imagesNoneActive[i]);
       }
        imageViews[position].setImageDrawable(imagesActive[position]);
        textViews[position].setTextColor(colorTabActive);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llMeetUp:
                viewPager.setCurrentItem(0);
                break;
            case R.id.llFindWho:
                viewPager.setCurrentItem(1);
                break;
            case R.id.llMyProfile:
                viewPager.setCurrentItem(2);
                break;
            case R.id.llAgenda:
                viewPager.setCurrentItem(3);
                break;
            case R.id.llContacts:
                viewPager.setCurrentItem(4);
                break;
        }
    }
}
